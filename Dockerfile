# syntax=docker/dockerfile:1

FROM ubuntu
MAINTAINER HASSAN WAHBA

RUN apt-get update -y

RUN apt-get update && apt-get install -y python3.9 python3-distutils python3-pip python3-apt

RUN apt-get install -y git

RUN git clone https://git.rwth-aachen.de/hassan-a-w/resource-based-conformance-checking-in-celonis.git

WORKDIR /resource-based-conformance-checking-in-celonis
RUN pip install --extra-index-url=https://pypi.celonis.cloud/ pycelonis=="1.7.4"
RUN pip install -r requirements.txt

COPY . /resource-based-conformance-checking-in-celonis

EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["src/app.py"]
