Overview
=========
This project can be used to analyze event logs for esource-based conformance checking, which contains different functional modules such as: 

* Extract the handover of work.  
* Identify resources causing high rework.  
* Identify duplicate activities across different resources.  
* Analyze the workload of the resources. 
* Identifying and defining several types of batch processing.  
* Discover deviations in the collaboration patterns.  
* Analyze resource-activity performance.  

and so on.
