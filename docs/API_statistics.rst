API-Statistics
===============



throughput_time
----------------
* A role is a set of activities in the log that are executed by a similar (multi)set of resources.  

* return
    * Dataframe



high_rework
----------------
* Extract Resource cause the high reworks.

* Parameters
    * quantile: Number

    Setting percentile of the activites occur. Default=0.99

* Returns
    * Dataframe
    
    List activities-resources that occur over the setting percentile.


high_rework_precision
----------------------
* Extract Resource cause the high reworks.

* Parameters
    * quantile: Number

    Setting percentile of the activities occur. Default=0.99

* Returns
    * Dataframe

        The precision of high rework with percentile.
    