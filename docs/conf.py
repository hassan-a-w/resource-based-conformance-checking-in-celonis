# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Resource-Based Conformance Checking in Celonis'
copyright = '2022, Tianyang Wang, Xi Zheng, Mahdi Khorrami, Hassan Wahba'
author = 'Tianyang Wang Xi Zheng Mahdi Khorrami Hassan Wahba'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.viewcode',
              'sphinx.ext.githubpages',
              'myst_parser',
              'sphinx.ext.napoleon',
              'sphinxcontrib.autohttp.flask',
              'sphinxcontrib.httpdomain',
              'sphinxcontrib.autohttp.flaskqref'
            #   'numpydoc',
            #   'IPython.sphinxext.ipython_console_highlighting',
            #   'IPython.sphinexext.ipython_directive'
            ]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

import os
import sys

sys.path.insert(0, os.path.abspath('../src'))
# sys.path.insert(0, os.path.abspath('../src/test'))
sys.path.insert(0, os.path.abspath('../src/routes'))

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'bizstyle'
html_static_path = ['_static']
