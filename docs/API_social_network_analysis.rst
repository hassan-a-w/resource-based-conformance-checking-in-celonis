API-Social Network Analysis
============================



work_deviation
----------------
* The resource starts to execute activities it never executes usually.
* Rreturn
    * Dataframe 


batch_type
----------------
* Find the type of the batch.
* Rreturn
    * Dataframe 
    
    List all the type of the batch across resource and activity.



duplicate_activity
-------------------
* Find the duplicate activities across different resources.
* Rreturn
    * Dataframe 
    
    List duplicate activities across different resources.

roles_discovery
----------------
* A role is a set of activities in the log that are executed by a similar (multi)set of resources.
* Rreturn
    * Dataframe 
    
organizational_mining
----------------------
* Discovery of behavior-related information specific to an organizational group (e.g. which activities are done by the group?).
    If the "org:group" attribute be there in the events, which describing the group that performed the event, you can use this function.
* Rreturn
    * Dataframe 


resource_activity_matrix_absolute
----------------------------------
* Find the resource-activity matrix.
* Rreturn
    * Dataframe 
    
    Restructure Eventlog to Resource-activity Matrix.

resource_activity_matrix_percent
---------------------------------
* Find the resource-activity matrix.
* Rreturn
    * Dataframe 
    
    Restructure Eventlog to Resource-activity Matrix.

work_handover
----------------
* Extract the handover of works .
* Rreturn
    * Dataframe 
    
    List all the activity-resource when handover happened.

similar_activities
-------------------
* how much similar is the work pattern between two resources.
* Rreturn
    * Dataframe 

working_together
----------------
* Find the duplicate activities across different resources.
* Rreturn
    * Dataframe 
    
    List duplicate activities across different resources.

subcontracting
----------------
* Find the duplicate activities across different resources.
* Rreturn
    * Dataframe 
    
     List duplicate activities across different resources.