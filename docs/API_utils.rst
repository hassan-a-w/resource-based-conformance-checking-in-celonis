API-Utils
===============



fetch_celonis_settings
--------------------------
* Fetch celonis's settings

get_celonis_settings
----------------------
* Return
    CELONIS, DATAMODEL, DATAMODEL_NAME, CASE_COLUMN, ACTIVITY_COLUMN, TIMESTAMP_COLUMN, RESOURCE_COLUMN


mine_petri_net
----------------
* Call pm4py discover_petri_net_inductive

* Parameters
    * log, t=0.2

* Returns
    * net, im, fm, tree


get_activity_resource
----------------------
* Find the relationship between activities and resources.

* Returns
    * Dataframe

    Table with case id, Activity, Resource and occurrences.

get_activity_resource_precision
---------------------------------
* Find the relationship between activities and resources for model and log.

* Returns
    * Dataframe

    Table with case id, Activity, Resource and occurrences.

RAM_restructure
----------------------
* Find the resource-activity matrix.

* Returns
    * Dataframe

    Restructure Eventlog to Resource-activity Matrix.


resource_activity_execution_time_avg
----------------------------------------------
* Find the execution time of the activities for each resource.

* Returns
    * Dataframe

    Table with Activity, Resource and execution time.