.. Resource-Based Conformance Checking in Celonis documentation master file, created by
   sphinx-quickstart on Wed Nov 23 23:47:55 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Resource-Based Conformance Checking in Celonis's documentation!
==========================================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Overview <overview>
   Install <install>
   Config Structure <config>
   API reference <API>

 
