API-Performance
===============



hard_tasks
----------------
* This functions mines tasks that require more than one person to complete within a given case.

* Rreturn
    * Dataframe 
    
    That contains the Case ID, Activity name and the resources required.

most_efficient
----------------
* Find the most efficient resource based execution times of the activities.

* Returns
    * Dataframe
    
    Table with Activity, Resource and execution time.


least_efficient
----------------------
* Find the least efficient resource based execution times of the activities.

* Returns
    * Dataframe

    Table with Activity, Resource and execution time.
    