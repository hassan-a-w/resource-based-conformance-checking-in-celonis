Overview
=========
Conformance checking is a set of techniques belonging to the discipline of process mining that compares the behavior of event logs with models to identify anomalies and suggest optimizations for processes.

In the process event log, resource information records associated events. According to the resource compensation, the collaboration mode within and among organizational units can be found, and the work efficiency of resources can be analyzed.

This project can be used to analyze event logs for resource-based conformance checking, which provide the following main analysis services:

* Analyze resource-activity performance.
* Identify resources causing high rework.  
* Discover deviations in the work patterns.
* Identifying and defining several types of batch processing.  
* Discover deviations in the collaboration patterns.  

and so on.
