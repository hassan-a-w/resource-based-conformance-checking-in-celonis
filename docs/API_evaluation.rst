API-Evaluation
===============



footprint_quality_dimensions
-----------------------------
* This function calculates the quality dimension of the event log using a footprint matrix.
    The dimension are: Fitness, Precision, Generality, Simplicity
* Rreturn
    * Dataframe 
    
    That contains the 3 dimensional KPI values

alignment_quality_dimensions
-----------------------------
* This function calculates the quality dimension of the event log using alignments.
    The dimension are: Fitness, Precision, Generality, Simplicity

* Returns
    * Dataframe
    
    That contains the 3 dimensional KPI values

