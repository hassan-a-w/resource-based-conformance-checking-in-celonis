# Resource-Based Conformance Checking in Celonis

## [Documentation](https://oomoomoo.github.io/Lab-conformance-checking/index.html)

- Overview
- Installation
- [API reference](https://oomoomoo.github.io/Lab-conformance-checking/API.html)

## Build Docker image
On the root directory, run

`docker image build -t flask_docker .`

to build the docker image of this application.

Start a container service with the command

`docker run -p 5000:5000 -d flask_docker`

Then Access the service on the specified port (i.e. 5000)
In any browser, type `http://127.0.0.1:5000` or `localhost:5000`

The first Step would be to add the celonis credentials at the `/login` endpoint.

## File Structure
Home

|--docs                         : Documentation  
|--src                          : Source-Code folder  
|--|--routes                    : Folder to define blueprints for the flask app  
|--|--|--analysis.py            : Python file that encapsulates analysis functions and endpoints  
|--|--|--handover.py            : Python file that encapsulates handover functions and endpoints  
|--|--|--petri_net.py           : Python file that encapsulates petri_net functions and endpoints  
|--|--templates                 : Folder for html and css templates  
|--|--|--dataframe.html         : HTML for the dataframe endpoints  
|--|--|--login.html             : HTML for the login page  
|--|--|--style.css              : CSS for the login page  
|--|--test                      : Test folder  
|--requirements.txt             : Required library used in this framework  
|--config.yaml                  : Celonis connection information  
|--Dockerfile                   : Docker image build configuration  
|--README.md                    : Intro to the project  


## Simple instruction for main functions
performance.least_efficient : Find the least efficient resource based execution times of the activities.

performance.most_efficient ：Find the most efficient resource based execution times of the activities.

handover.resource_activity_matrix_absolute : Find the resource-activity matrix (absolute).

handover.resource_activity_matrix_percent ：Find the resource-activity matrix （percentpercent）.

handover.work_handover ：Extract the handover of works .

analysis.duplicate_activity : Find the duplicate activities across different resources.

analysis.roles_discovery : A role is a set of activities in the log that are executed by a similar (multi)set of resources.

analysis.work_deviation : The resource starts to execute activities it never executes usually.

analysis.hard_tasks : This functions mines tasks that require more than one person to complete within a given case

analysis.batch_type : Find the type of the batch.

petri_net.mine ：This function mines the event log given in the job configuration

high_rework_quantil_90 : Extract Resource cause the high reworks (Number of work-occurrences is over the 90th percentil)(try to make the percentile can be adjusted in 3rd sprint ) .

more details see: [API reference](https://oomoomoo.github.io/Lab-conformance-checking/API.html)
