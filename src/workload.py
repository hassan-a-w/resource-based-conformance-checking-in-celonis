from pycelonis.celonis_api.pql.pql import PQL, PQLColumn


def workload_data(model_name,model):
    """Find the relationship between activities depend on resources.
    
    :param model_name: Name of the model in Celonis.
    :param model: Model in Celonis.
    :return: Dataframe
    """
    datamodel = model.datamodels.find(model_name)
    query0 = PQL()
    query0.add(PQLColumn(name="source", query=f"SOURCE(\"{model_name}\".\"concept:name\")"))
    query0.add(PQLColumn(name="target", query=f"TARGET(\"{model_name}\".\"concept:name\")"))

    query0.add(PQLColumn(name="source-resource", query=f"SOURCE(\"{model_name}\".\"org:RESOURCE\")"))
    query0.add(PQLColumn(name="target-resource", query=f"TARGET(\"{model_name}\".\"org:RESOURCE\")"))

    query0.add(PQLColumn(name="source-time", query=f"SOURCE(\"{model_name}\".\"time:TIMESTAMP\")"))
    query0.add(PQLColumn(name="target-time", query=f"TARGET(\"{model_name}\".\"time:TIMESTAMP\")"))
    df0 = datamodel.get_data_frame(query0)
    return df0

def workload(model_name,model):
    """Find the workload of resources.
       
    :param model_name: Name of the model in Celonis.
    :param model: Model in Celonis.
    :return: Dataframe
    """
    df=workload_data(model_name,model)
    df['workload']=df[["target-time","source-time"]].apply(lambda x:x["target-time"]-x["source-time"],axis=1)
    result=df.groupby(['source-resource'])['workload'].sum().reset_index()
    return result

def workload_detail(model_name,model):
    """Find the workload of resources and the activities depend on this resource.
        
    :param model_name: Name of the model in Celonis.
    :param model: Model in Celonis.
    :return: Dataframe
    """
    df=workload_data(model_name,model)
    df['workload']=df[["target-time","source-time"]].apply(lambda x:x["target-time"]-x["source-time"],axis=1)
    result=df.groupby(['source-resource','source','target'])['workload'].sum().reset_index()
    return result
