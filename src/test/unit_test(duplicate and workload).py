
def is_duplicate(result):
    for row in result.itertuples():
        if getattr(row, 'Occurrences')<2:
            return False
        
    return True


