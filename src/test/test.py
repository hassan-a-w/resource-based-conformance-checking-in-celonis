import pandas as pd
from pandas.testing import assert_frame_equal  # <-- for testing dataframes


def test_dataframe_bound(self, test_column, i, is_lower_bound=True, contain_equal=True):
    """Test the bound of a column in Dataframe.
    
    Parameters
    ----------
    result : Dataframe
        Test Dataframe.
    test_column: String
        Name of the test column.
    i : number
        Setting the upper/Lower bound of the column.
    is_lower_bound: Booean
        Decide test value i is lower bound or upper bound. Default=True/lower bound
    contain_equal: Booean
        contian equal or not. Default=True
    
    Returns
    -------
    Boolean
    """

    if is_lower_bound and contain_equal:
        for row in self.itertuples():
            if getattr(row, test_column) < i:
                assert False, "There are some values below the lower bound"
    elif is_lower_bound and not contain_equal:
        for row in self.itertuples():
            if getattr(row, test_column) <= i:
                assert False, "There are some values below the lower bound"
    elif not is_lower_bound and contain_equal:
        for row in self.itertuples():
            if getattr(row, test_column) > i:
                assert False, "There are some values over the upper bound"
    else:
        for row in self.itertuples():
            if getattr(row, test_column) >= i:
                assert False, "There are some values over the upper bound"
    return True


def test_is_dataframe(self):
    """Test whether the datatype of a variable is Dataframe.
    
    Parameters
    -------
    
    Returns
    -------
    Boolean
    """
    if isinstance(self, pd.DataFrame):
        return True
    else:
        assert False, "The datatype is not Dataframe"
