from utils import fetch_celonis_settings, is_loaded, do_load
from flask import Flask, render_template, request, redirect
from routes.social_network_analysis import social_network_analysis_app
from routes.performance import performance_app
from routes.evaluation import evaluation_app
from routes.petri_net import petri_app
from routes.statistics import statistics_app


app = Flask('Resource-Based Conformance Checking',
            template_folder="templates",
            static_folder="static")
app.register_blueprint(petri_app, url_prefix='/miner/inductive')
app.register_blueprint(social_network_analysis_app, url_prefix='/analysis/social_network_analysis')
app.register_blueprint(evaluation_app, url_prefix='/analysis/evaluation')
app.register_blueprint(performance_app, url_prefix='/analysis/performance')
app.register_blueprint(statistics_app, url_prefix='/analysis/statistics')


@app.route("/kpis")
def kpis():
    if is_loaded():
        return render_template("kpis.html")
    else:
        return redirect('/')


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/")
def home():
    if not is_loaded():
        error = fetch_celonis_settings()
        if error:
            msg = 'Stored settings are not a correct combination, given:\n' + str(error)
            return render_template("error.html", text=msg)
        else:
            do_load(True)

    return render_template("home.html")


@app.route("/login", methods=["GET", "POST"])
def login_page():
    if request.method == "POST":
        case_column = request.form['case_column']
        activity_column = request.form['activity_column']
        timestamp_column = request.form['timestamp_column']
        res_col = request.form['resource_column']
        if request.form['case_column'] == "":
            case_column = "case:concept:name"
        if request.form['activity_column'] == "":
            activity_column = "concept:name"
        if request.form['timestamp_column'] == "":
            timestamp_column = "time:timestamp"
        if request.form['resource_column'] == "":
            res_col = "org:resource"

        do_load(False)

        content = """authorize:
  {{ celonis_url : {url},
    api_token : {api_key}
}}
data_model: {data_model}
case_column: {case_column}
activity_column: {activity_column}
timestamp_column: {timestamp_column}
resource_column: {res_col}""".format(url=request.form['url'],
                                   api_key=request.form['api_key'],
                                   data_model=request.form['data_model'],
                                   case_column=case_column,
                                   activity_column=activity_column,
                                   timestamp_column=timestamp_column,
                                   res_col=res_col)
        f = open("../.config.yaml", "w")
        f.write(content)
        f.close()
        return redirect("/")
    return render_template("login.html")


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
