import yaml
import pm4py
import pandas as pd
import numpy as np

from pycelonis import get_celonis
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn

LOADED = False

CELONIS = None
DATAMODEL = None
DATAMODEL_NAME = None
CASE_COLUMN = None
ACTIVITY_COLUMN = None
TIMESTAMP_COLUMN = None
RESOURCE_COLUMN = None


def fetch_celonis_settings():
    try:
        config_file = open('../.config.yaml')
        config_dict = yaml.safe_load(config_file)
    except FileNotFoundError:
        return 'Empty File'

    try:
        celonis = get_celonis(url=config_dict['authorize']['celonis_url'],
                              api_token=config_dict['authorize']['api_token'])
    except:
        return """
URL and API Key are not a valid combination:
Celonis URL: {}
API Key Length: {}
        """.format(config_dict['authorize']['celonis_url'],
                   len(config_dict['authorize']['api_token']))

    if len(celonis.datamodels.data) == 0:
        return """
URL and API Key are not a valid combination:
Celonis URL: {}
API Key Length: {}
        """.format(config_dict['authorize']['celonis_url'],
                   len(config_dict['authorize']['api_token']))

    datamodel_names = [x.name for x in celonis.datamodels.data]
    datamodel_name = config_dict['data_model']
    if datamodel_name not in datamodel_names:
        return """
Data Model does not exist, given: {}. Expected {}
        """.format(config_dict['data_model'], str(datamodel_names))

    datamodel = celonis.datamodels.find(datamodel_name)
    case_column = config_dict['case_column']
    activity_column = config_dict['activity_column']
    timestamp_column = config_dict['timestamp_column']
    resource_column = config_dict['resource_column']
    column_names = [x['name'] for x in datamodel.default_activity_table.columns]
    if case_column not in column_names:
        return """
        Case Column does not exist, given: {}. Expected: {}
                """.format(case_column, str(column_names))
    if activity_column not in column_names:
        return """
        Activity Column does not exist, given: {}. Expected: {}
                """.format(activity_column, str(column_names))
    if timestamp_column not in column_names:
        return """
        Timestamp Column does not exist, given: {}. Expected: {}
                """.format(timestamp_column, str(column_names))
    if resource_column not in column_names:
        return """
        Resource Column does not exist, given: {}. Expected: {}
                """.format(resource_column, str(column_names))
    global LOADED, CELONIS, DATAMODEL, DATAMODEL_NAME, CASE_COLUMN, ACTIVITY_COLUMN, TIMESTAMP_COLUMN, RESOURCE_COLUMN
    CELONIS, DATAMODEL, DATAMODEL_NAME, CASE_COLUMN, ACTIVITY_COLUMN, TIMESTAMP_COLUMN, RESOURCE_COLUMN = celonis, datamodel, datamodel_name, case_column, activity_column, timestamp_column, resource_column
    return False


def get_celonis_settings():
    if CELONIS is None:
        raise ValueError()
    return CELONIS, DATAMODEL, DATAMODEL_NAME, CASE_COLUMN, ACTIVITY_COLUMN, TIMESTAMP_COLUMN, RESOURCE_COLUMN


def mine_petri_net(log, t=0.2):
    net, im, fm = pm4py.discover_petri_net_inductive(log, noise_threshold=t)
    tree = pm4py.convert_to_process_tree(net, im, fm)
    return net, im, fm, tree


def get_activity_resource():
    """Find the relationship between activities and resources.

    Returns
    -------
    Dataframe
        Table with case id, Activity, Resource and occurrences

    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        raise ValueError()

    query = PQL()
    query.add(PQLColumn(name="caseId", query=f'SOURCE("{model_name}"."{case_column}")'))
    query.add(PQLColumn(name="source", query=f'SOURCE("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="target", query=f'TARGET("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="source-resource", query=f'SOURCE("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="target-resource", query=f'TARGET("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="occurrences", query=f'COUNT(SOURCE("{model_name}"."{activity_column}"))'))
    df = datamodel.get_data_frame(query)
    return df

def get_activity_resource_precision():
    """Find the relationship between activities and resources for model and log.

    Returns
    -------
    Dataframe
        Table with case id, Activity, Resource and occurrences

    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        raise ValueError()

    query = PQL()
    query.add(PQLColumn(name="caseId", query=f'SOURCE("{model_name}"."{case_column}")'))
    query.add(PQLColumn(name="Activity", query=f'SOURCE("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="resource", query=f'SOURCE("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="occurrences", query=f'COUNT(SOURCE("{model_name}"."{activity_column}"))'))
    df = datamodel.get_data_frame(query)

    query1 = PQL()
    query1.add(PQLColumn(name="caseId", query=f'("{model_name}"."{case_column}")'))
    query1.add(PQLColumn(name="Activity", query=f'("{model_name}"."{activity_column}")'))
    query1.add(PQLColumn(name="resource", query=f'("{model_name}"."{resource_column}")'))
    query1.add(PQLColumn(name="occurrences", query=f'COUNT(("{model_name}"."{activity_column}"))'))
    df1 = datamodel.get_data_frame(query1)
    return df,df1

def RAM_restructure(sum=False, absolute=False):
    """Find the resource-activity matrix.

    Returns
    -------
    Dataframe
        Restructure Eventlog to Resource-activity Matrix.
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        raise ValueError()

    query0 = PQL()
    query0.add(PQLColumn(name="caseId", query=f'TARGET("{model_name}"."{case_column}")'))
    query0.add(PQLColumn(name="activity", query=f'TARGET("{model_name}"."{activity_column}")'))
    query0.add(PQLColumn(name="resource", query=f'TARGET("{model_name}"."{resource_column}")'))
    df = datamodel.get_data_frame(query0)

    query0 = PQL()
    query0.add(PQLColumn(name="activity", query=f'"{model_name}"."{activity_column}"'))
    query0.add(PQLColumn(name="count", query=f'COUNT("{model_name}"."{activity_column}")'))
    df_grouped = datamodel.get_data_frame(query0)

    if sum:
        df1 = df.groupby(['resource', 'activity'], group_keys=True)['caseId'].apply(
            lambda x: x.count()).reset_index()
    else:
        df1 = df.groupby(['resource', 'activity'], group_keys=True)['caseId'].apply(
            lambda x: (x + ', ').sum()).reset_index()

    index = df1['resource'].drop_duplicates(keep='first')
    col = df1['activity'].drop_duplicates(keep='first')
    matrix = pd.DataFrame(np.nan, index=list(index), columns=list(col))

    for resource, activity, value in df1.values:
        if sum:
            num_of_occ = df_grouped.loc[df_grouped['activity'] == activity]['count'].item()
            value = str(value) if absolute else "{:.3f}".format(value / num_of_occ)

        matrix.loc[resource, activity] = value

    return matrix, index, col


def resource_activity_execution_time_avg():
    """Find the execution time of the activities for each resource.

    Returns
    -------
    Dataframe
        Table with Activity, Resource and execution time

    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        raise ValueError()

    query = PQL()
    # query.add(PQLColumn(name="caseId", query=f"SOURCE("{model_name}\"."{case_column}")"))
    query.add(PQLColumn(name="Activity", query=f'TARGET("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="Resource", query=f'TARGET("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="Execution Time",
                        query=f'sum(minutes_between(SOURCE("{model_name}"."{timestamp_column}"),TARGET(\"{model_name}\"."{timestamp_column}")))/count(SOURCE(\"{model_name}\"."{activity_column}"))'))
    df = datamodel.get_data_frame(query)
    return df


def is_loaded():
    global LOADED
    return LOADED


def do_load(load):
    global LOADED
    LOADED = load
