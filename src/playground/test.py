import pandas as pd
import numpy as np
from pycelonis import get_celonis
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn,PQLFilter
import yaml
import pm4py
import os

config_file = open('config.yaml')
CONF = yaml.safe_load(config_file)

c = get_celonis(url=CONF['authorize']['celonis_url'], api_token=CONF['authorize']['api_token'],key_type='APP_KEY')

print( c.datamodels)

model_name="Receipt"
datamodel = c.datamodels.find(f"{model_name}")
query = PQL()
query.add(PQLColumn(name="case:concept:name", query=f"(\"{model_name}\".\"case id\")"))
query.add(PQLColumn(name="concept:name", query=f"(\"{model_name}\".\"concept:name\")"))
query.add(PQLColumn(name="org:resource", query=f"(\"{model_name}\".\"org:RESOURCE\")"))
query.add(PQLColumn(name="time:timestamp", query=f"(\"{model_name}\".\"time:TIMESTAMP\")"))
df = datamodel.get_data_frame(query)

wt_values = pm4py.discover_subcontracting_network(df)
result = pm4py.view_sna(wt_values)