import pandas as pd
import numpy as np
from pycelonis import get_celonis
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn,PQLFilter
import yaml
import os

# +
config_file = open('config.yaml')
CONF = yaml.safe_load(config_file)

c = get_celonis(url=CONF['authorize']['celonis_url'], api_token=CONF['authorize']['api_token'],key_type='APP_KEY')

print( c.datamodels)
# -
model_name="Receipt"
datamodel = c.datamodels.find(f"{model_name}")
query0 = PQL()
query0.add(PQLColumn(name="caseId", query=f"SOURCE(\"{model_name}\".\"case id\")"))
query0.add(PQLColumn(name="activity", query=f"SOURCE(\"{model_name}\".\"concept:name\")"))
query0.add(PQLColumn(name="resource", query=f"SOURCE(\"{model_name}\".\"org:RESOURCE\")"))
df = datamodel.get_data_frame(query0)
df


# +
case_sum=df.nunique().caseId

df1 = df.groupby(['resource','activity'], group_keys=True)['caseId'].apply(lambda x: (x.count())/case_sum).reset_index()
df1

# +

value=df1.values
index=df1['resource'].drop_duplicates(keep='first')
col=df1['activity'].drop_duplicates(keep='first')
matrix=pd.DataFrame(np.nan,index=list(index),columns=list(col))
for i in value:
    value1=i[0]
    value2=i[1]
    value3=i[2]
    matrix.loc[value1,value2]=value3

matrix

# +
model_name="Receipt"
datamodel = c.datamodels.find(f"{model_name}")
query1 = PQL()
query1.add(PQLColumn(name="caseId", query=f"SOURCE(\"{model_name}\".\"case id\")"))
query1.add(PQLColumn(name="activity", query=f"SOURCE(\"{model_name}\".\"concept:name\")"))
query1.add(PQLColumn(name="resource", query=f"SOURCE(\"{model_name}\".\"org:RESOURCE\")"))

query1.add(PQLColumn(name="start-time", query=f"SOURCE(\"{model_name}\".\"time:TIMESTAMP\")"))
query1.add(PQLColumn(name="end-time", query=f"TARGET(\"{model_name}\".\"time:TIMESTAMP\")"))
df0 = datamodel.get_data_frame(query1)
df0
# -

freq = '1min'
df0['start-time'] = df0['start-time'].dt.round(freq)
df0['end-time'] = df0['end-time'].dt.round(freq)
df0

# +
batch_sim=[]
batch_seq=[]
batch_con=[]
batch_non=[]

for y in col:
    #print(y)
    
    list1=np.where(matrix[y].isna())
    index2=index.tolist()
    index_value=[]
    #print(index2)
    for i in list1[0]:
        
        index_value.append(index2[i])
    #print(index_value)
    for z in index_value:
        index2.remove(z)
    #print(index2)  
    for x in index2:
        #print(x)
        g_r=df0.groupby(['resource']).get_group(x)
        #print(g_r)
        g_ra=g_r.groupby(['activity']).get_group(y)
        value=g_ra.values
        #print(value)
        
        for i in value:
            #print(i)
            for j in value:
                
                if (i!=j).any():
                    #print(i[3])
                    #print(j[3])
                    if (i[3]==j[3])&(i[4]==j[4]):
                        
                        #print(i[0]+'sim'+j[0])
                        batch_sim.append(i[2])
                    elif (i[4]==j[3])|(j[4]==i[3]):
                        #print(i[0]+'seq'+j[0])
                        batch_seq.append(i[2])
                    elif (i[4]<j[3])|(j[4]<i[3]):
                        #print(i[0]+'no relationship'+j[0])
                        batch_non.append(i[2])
                    else:
                        #print(i[0]+'con'+j[0])
                        batch_con.append(i[2])
                else:
                    pass
    
 
    df_sim = pd.DataFrame(batch_sim, columns=['Simultaneous batch:']).drop_duplicates()
    print(df_sim)

    df_seq = pd.DataFrame(batch_seq, columns=['Sequential batch:']).drop_duplicates()
    print(df_seq)

    df_con = pd.DataFrame(batch_con, columns=['Concurrent batch:']).drop_duplicates()
    print(df_con)
    #print(batch_non)

# +
batch_sim=[]
batch_seq=[]
batch_con=[]
batch_non=[]

for y in col:
    #print(y)
    
    list1=np.where(matrix[y].isna())
    index2=index.tolist()
    index_value=[]
    #print(index2)
    for i in list1[0]:
        
        index_value.append(index2[i])
    #print(index_value)
    for z in index_value:
        index2.remove(z)
    #print(index2)  
    for x in index2:
        #print(x)
        g_r=df0.groupby(['resource']).get_group(x)
        #print(g_r)
        g_ra=g_r.groupby(['activity']).get_group(y)
        value=g_ra.values
        #print(value)
        
        for i in value:
            #print(i)
            for j in value:
                
                if (i!=j).any():
                    #print(i[3])
                    #print(j[3])
                    if (i[3]==j[3])&(i[4]==j[4]):
                        
                        #print(i[0]+'sim'+j[0])
                        batch_sim.append(i[2])
                    elif (i[4]==j[3])|(j[4]==i[3]):
                        #print(i[0]+'seq'+j[0])
                        batch_seq.append(i[2])
                    elif (i[4]<j[3])|(j[4]<i[3]):
                        #print(i[0]+'no relationship'+j[0])
                        batch_non.append(i[2])
                    else:
                        #print(i[0]+'con'+j[0])
                        batch_con.append(i[2])
                else:
                    pass
                
    df_sim = pd.DataFrame(batch_sim, columns=['Simultaneous batch:']).drop_duplicates()
    df_seq = pd.DataFrame(batch_seq, columns=['Sequential batch:']).drop_duplicates()
    df_con = pd.DataFrame(batch_con, columns=['Concurrent batch:']).drop_duplicates()
print(df_sim)    
print(df_seq) 
print(df_con)
# -

df2 = pd.DataFrame({'id': 'a b c x y z'.split(),
                   'act': ['d','d','d','d','d','e'],
                   'resource': ['1','1','1','1','1','3'],
                   'start': ['2011-08-11 07:26:00','2011-08-11 07:26:00','2011-08-11 07:26:00','2011-08-11 07:27:00','5','6'],
                   'end': ['2011-08-11 07:28:00','2011-08-11 07:28:00','2011-08-11 07:27:00','2011-08-11 07:28:00','6','6']})
df2

# +
id=['a','b']
act='d'
res='1'

g_r=df2.groupby(['resource']).get_group(res)
g_ra=g_r.groupby(['act']).get_group(act)
value=g_ra.values
value

batch_sim=[]
batch_seq=[]
batch_con=[]

for i in value:
    for j in value:
        if (i!=j).any():
            if (i[3]==j[3])&(i[4]==j[4]):
                print(i[0]+'sim'+j[0])
                batch_sim.append(i[2])
            elif (i[4]==j[3])|(j[4]==i[3]):
                print(i[0]+'seq'+j[0])
                batch_seq.append(i[2])
            elif (i[4]<j[3])|(j[4]<i[3]):
                print(i[0]+'no relationship'+j[0])
            else:
                print(i[0]+'con'+j[0])
                batch_con.append(i[2])
        else:
            pass
    
print(batch_sim)
print(batch_seq)
print(batch_con)

# +
g1 = df2.groupby(['A','B'], group_keys=True)['C'].apply(lambda x: (x+',').sum()).reset_index()

print(g1)
# -

g2=g1.values
g2[0]

# +
index=[0,1,2,3,4,5]

list1=np.where(df2['end'].isna())


# -

columns=['id','act','resource','start','end']
index=[0,1,2,3,4,5]
for i in columns:
    list1=np.where(df2[i].isna())
    for i in list1[0]:
        index.remove(i)
    print(index)

index=g1['A'].drop_duplicates(keep='first')
col=g1['B'].drop_duplicates(keep='first')
data=pd.DataFrame(np.nan,index=list(index),columns=list(col))
data

# +
for i in g2:
    value1=i[0]
    value2=i[1]
    value3=i[2]
    data.loc[value1,value2]=value3
    
data

# +
#data['a'][0]

# +
#data.loc['b','a']=1
#data
# -




