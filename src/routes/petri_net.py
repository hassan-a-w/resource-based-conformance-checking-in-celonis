from flask import Blueprint, send_file, redirect
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn
from pm4py.visualization.petri_net import visualizer as pn_visualizer
import inspect
import sys
import os

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from utils import mine_petri_net, get_celonis_settings


petri_app = Blueprint('petri_net', __name__)


@petri_app.route('/visualize')
def mine():
    """
    This function mines the event log given in the job configuration (see /login)
    :return:
        Petri Net image annotated with case absolute frequency
    """
    filename = "inductive_frequency.png"
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    df = datamodel.get_data_frame(query)
    net, im, fm, tree = mine_petri_net(df)

    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = pn_visualizer.apply(net, im, fm, parameters=parameters,
                               variant=pn_visualizer.Variants.FREQUENCY, log=df)
    pn_visualizer.save(gviz, filename)

    return send_file(filename, mimetype='image/gif')
