import pandas as pd
from pm4py.algo.evaluation.generalization import algorithm as generalization_evaluator
from pm4py.algo.evaluation.simplicity import algorithm as simplicity_evaluator
from flask import Blueprint, redirect, render_template
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn
import numpy as np
import inspect
import pm4py
import sys
import os

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from utils import mine_petri_net, get_celonis_settings, RAM_restructure

evaluation_app = Blueprint('evaluation', __name__)


@evaluation_app.route('/footprint_quality_dimensions')
def footprint_quality_dimensions():
    """
    This function calculates the quality dimension of the event log using a footprint matrix.
    The dimension are: Fitness, Precision, Generality, Simplicity
    :return: a dataframe that contains the 3 dimensional KPI values
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    log = datamodel.get_data_frame(query)
    net, im, fm, _ = mine_petri_net(log)
    fitness = pm4py.fitness_footprints(log, net, im, fm)
    prec = pm4py.precision_footprints(log, net, im, fm)
    gen = generalization_evaluator.apply(log, net, im, fm)
    simp = simplicity_evaluator.apply(net)


    x = np.array([
        ['Fitness', 'Precision', 'Generality', 'Simplicity'],
        [fitness, prec, gen, simp]
    ]).transpose()

    title = pd.DataFrame(x, columns=['Dimension', 'Value'])

    return render_template('dataframe.html', tables=[title.to_html(classes='data', index=False)], titles=title.columns.values)


@evaluation_app.route('/alignment_quality_dimensions')
def alignment_quality_dimensions():
    """
    This function calculates the quality dimension of the event log using alignments.
    The dimension are: Fitness, Precision, Generality, Simplicity
    :return: a dataframe that contains the 3 dimensional KPI values
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    log = datamodel.get_data_frame(query)
    net, im, fm, _ = mine_petri_net(log)
    fitness = pm4py.fitness_alignments(log, net, im, fm)
    prec = pm4py.precision_alignments(log, net, im, fm)
    gen = generalization_evaluator.apply(log, net, im, fm)
    simp = simplicity_evaluator.apply(net)

    x = np.array([
        ['Dimension', 'Fitness', 'Precision', 'Generality', 'Simplicity'],
        ['Value', fitness, prec, gen, simp]
    ]).transpose()

    title = pd.DataFrame(x, columns=['Dimension', 'Value'])

    return render_template('dataframe.html', tables=[title.to_html(classes='data', index=False)], titles=title.columns.values)

