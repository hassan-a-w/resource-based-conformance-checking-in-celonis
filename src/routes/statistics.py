from flask import Blueprint, render_template, redirect, send_file, request
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn
from pm4py.statistics.traces.generic.log import case_arrival
import pandas as pd
import inspect
import pm4py
import sys
import os

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from utils import mine_petri_net, get_celonis_settings, RAM_restructure, get_activity_resource, get_activity_resource_precision

statistics_app = Blueprint('statistics', __name__)


@statistics_app.route('/throughput_time')
def throughput_time():
    """A role is a set of activities in the log that are executed by a similar (multi)set of resources.
    :return: Dataframe
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    datamodel = model.datamodels.find(model_name)
    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    log = datamodel.get_data_frame(query)
    # logic
    all_case_durations = pm4py.get_all_case_durations(log=log,
                                                      activity_key="concept:name",
                                                      case_id_key="case:concept:name",
                                                      timestamp_key="time:timestamp")

    avg_tpt = sum(all_case_durations) / len(all_case_durations)

    case_arrival_ratio = pm4py.get_case_arrival_average(log=log,
                                                        activity_key="concept:name",
                                                        case_id_key="case:concept:name",
                                                        timestamp_key="time:timestamp")

    case_dispersion_ratio = case_arrival.get_case_dispersion_avg(log, parameters={
        case_arrival.Parameters.TIMESTAMP_KEY: "time:timestamp",
        case_arrival.Parameters.CASE_ID_KEY: "case:concept:name",
        case_arrival.Parameters.ACTIVITY_KEY: "concept:name"
    })

    data = [['Average Throughput Time', avg_tpt],
            ['Case Arrival Ratio', case_arrival_ratio],
            ['Case Dispersion Ratio', case_dispersion_ratio]]
    title = pd.DataFrame(data, columns=['Metric', 'Value'])

    return render_template('dataframe.html', tables=[title.to_html(classes='data', index=False)], titles=title.columns.values)


def performance_spectrum():
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    datamodel = model.datamodels.find(model_name)
    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    file_name = 'performance_spectrum.png'
    log = datamodel.get_data_frame(query)
    acts = log['concept:name'].unique()[1:]
    # acts = ['T06 Determine necessity of stop advice', 'T02 Check confirmation of receipt']
    # pm4py.view_performance_spectrum(log, acts)
    pm4py.save_vis_performance_spectrum(log, acts, file_name)
    pm4py.view_performance_spectrum()
    return send_file(file_name, mimetype='image/gif')


@statistics_app.route('/high_rework', methods=['GET', 'POST'])
def high_rework():
    """Extract Resource cause the high reworks.

    Parameters
    ----------
    quantile: Number
        Setting percentile of the activites occur. Default=0.99

    Returns
    -------
    Dataframe
        List activities-resources that occur over the setting percentile.
    """
    try:
        get_celonis_settings()
    except ValueError:
        return redirect('/')
    quantile = 0.99
    if request.method == "POST":
        quantile = request.form.get('percentile', type=float)
    try:
        df = get_activity_resource()
    except ValueError:
        return redirect('/')
    rework_quantile = df['occurrences'].quantile(q=quantile)
    df = df[df["occurrences"] > rework_quantile].sort_values(by="occurrences", ascending=0)
    return render_template('highrework.html', tables=[df.to_html(classes='data')], titles=df.columns.values)


@statistics_app.route('/high_rework_precision', methods=['GET', 'POST'])
def high_rework_precision():
    """Extract Resource cause the high reworks.

    Parameters
    ----------
    quantile: Number
        Setting percentile of the activities occur. Default=0.99

    Returns
    -------
    Dataframe
        The precision of high rework with percentile.
    """
    try:
        get_celonis_settings()
    except ValueError:
        return redirect('/')
    quantile = 0.99
    if request.method == "POST":
        quantile = request.form.get('percentile', type=float)
    try:
        model, log = get_activity_resource_precision()
    except ValueError:
        return redirect('/')
    rework_quantile = model['occurrences'].quantile(q=quantile)
    model = model[model["occurrences"] > rework_quantile].sort_values(by="occurrences", ascending=0)
    rework_quantile = log['occurrences'].quantile(q=quantile)
    log = log[log["occurrences"] > rework_quantile].sort_values(by="occurrences", ascending=0)
    tp = pd.merge(model, log, how='left', left_on=["caseId", "Activity", "resource", "occurrences"],
                  right_on=["caseId", "Activity", "resource", "occurrences"])
    if not len(log):
        return render_template("highrework_precision.html", msg=f"The high rework with percentile {quantile} results "
                                                                f"in an empty log.")
    precision = len(tp) / len(log)

    msg = f'The precision of high rework with percentile {quantile} is {precision}.'
    return render_template("highrework_precision.html", msg=msg)
