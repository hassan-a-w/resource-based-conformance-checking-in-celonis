import pandas as pd
from flask import Blueprint, render_template, redirect
import inspect
import sys
import os

from pycelonis.celonis_api.pql.pql import PQL, PQLColumn

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from utils import resource_activity_execution_time_avg, get_celonis_settings

performance_app = Blueprint('performance', __name__)


@performance_app.route('/hard_tasks')
def hard_tasks():
    """
    This functions mines tasks that require more than one person to complete within a given case
    :return:
        Dataframe that contains the Case ID, Activity name and the resources required
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="Case ID", query=f'SOURCE("{model_name}"."{case_column}")'))
    query.add(PQLColumn(name="Activity", query=f'SOURCE("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="target", query=f'TARGET("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="source-resource", query=f'SOURCE("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="target-resource", query=f'TARGET("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="occurrences", query=f'COUNT(SOURCE("{model_name}"."{activity_column}"))'))
    df = datamodel.get_data_frame(query)

    # logic
    result = df[(df["Activity"] == df["target"]) & (df["source-resource"] != df["target-resource"])]

    result_1 = result[["Case ID", "Activity", "source-resource"]]
    result_1.rename(columns={'source-resource': 'Resource'}, inplace=True)

    result_2 = result[["Case ID", "Activity", "target-resource"]]
    result_2.rename(columns={'target-resource': 'Resource'}, inplace=True)

    df_all_rows = pd.concat([result_1, result_2], ignore_index=True).drop_duplicates()

    df_grouped = df_all_rows.groupby(["Case ID", "Activity"])["Resource"].apply(list)
    df_to_html = pd.DataFrame(df_grouped)
    if not df_to_html.empty:
        return render_template('dataframe.html', tables=[df_to_html.to_html(classes='data')],
                               titles=df_to_html.columns.values)
    else:
        return render_template("empty.html", msg="Good news! No tasks require more than 1 worker.")


@performance_app.route('/most_efficient')
def most_efficient():
    """Find the most efficient resource based execution times of the activities.
    
    Returns
    -------
    Dataframe
        Table with Activity, Resource and execution time   
    """
    df = resource_activity_execution_time_avg()
    result=df.groupby(['Activity']).min()
    return render_template('dataframe.html',  tables=[result.to_html(classes = 'data')], titles=result.columns.values)


@performance_app.route('/least_efficient')
def least_efficient():
    """Find the least efficient resource based execution times of the activities.
    
    Returns
    -------
    Dataframe
        Table with Activity, Resource and execution time   
    """
    df = resource_activity_execution_time_avg()
    result=df.groupby(['Activity']).max()
    return render_template('dataframe.html',  tables=[result.to_html(classes = 'data')], titles=result.columns.values)
