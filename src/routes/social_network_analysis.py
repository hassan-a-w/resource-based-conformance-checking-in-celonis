from flask import Blueprint, render_template, redirect
from pycelonis.celonis_api.pql.pql import PQL, PQLColumn
from pm4py.algo.organizational_mining.local_diagnostics import algorithm as local_diagnostics
import pandas as pd
import numpy as np
import inspect
import pm4py
import sys
import os

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
from utils import get_activity_resource, mine_petri_net, get_celonis_settings, RAM_restructure, get_activity_resource_precision

social_network_analysis_app = Blueprint('social_network_analysis', __name__)


@social_network_analysis_app.route('/work_deviation')
def work_deviation():
    """ The resource starts to execute activities it never executes usually.
    :return: Dataframe
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="activity", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="occurrences", query=f'COUNT("{model_name}"."{activity_column}")'))

    df = datamodel.get_data_frame(query)
    result = df[df["occurrences"] == 1].sort_values(by='resource', ascending=0)
    return render_template('dataframe.html', tables=[result.to_html(classes='data')], titles=result.columns.values)


@social_network_analysis_app.route('/batch_type')
def batch_type():
    """Find the type of the batch.

    Returns
    -------
    Dataframe
        List all the type of the batch across resource and activity
    """

    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="caseId", query=f'TARGET("{model_name}"."{case_column}")'))
    query.add(PQLColumn(name="activity", query=f'TARGET("{model_name}"."{activity_column}")'))
    query.add(PQLColumn(name="resource", query=f'TARGET("{model_name}"."{resource_column}")'))
    query.add(PQLColumn(name="start-time", query=f'SOURCE("{model_name}"."{timestamp_column}")'))
    query.add(PQLColumn(name="end-time", query=f'TARGET("{model_name}"."{timestamp_column}")'))
    df0 = datamodel.get_data_frame(query)

    try:
        matrix, index, col = RAM_restructure(sum=True, absolute=True)
    except ValueError:
        return redirect('/')

    freq = '1min'
    df0['start-time'] = df0['start-time'].dt.round(freq)
    df0['end-time'] = df0['end-time'].dt.round(freq)

    batch_sim = []
    batch_seq = []
    batch_con = []
    batch_non = []

    for y in col:
        list1 = np.where(matrix[y].isna())
        index2 = index.tolist()
        index_value = []

        for i in list1[0]:
            index_value.append(index2[i])

        for z in index_value:
            index2.remove(z)

        for x in index2:
            g_r = df0.groupby(['resource']).get_group(x)
            g_ra = g_r.groupby(['activity']).get_group(y)
            value = g_ra.values

            for i in value:
                for j in value:
                    if (i != j).any():
                        if (i[3] == j[3]) & (i[4] == j[4]):
                            batch_sim.append(i[2])

                        elif (i[4] == j[3]) | (j[4] == i[3]):
                            batch_seq.append(i[2])

                        elif (i[4] < j[3]) | (j[4] < i[3]):
                            batch_non.append(i[2])

                        else:
                            batch_con.append(i[2])
                    else:
                        pass

        df_sim = pd.DataFrame(batch_sim, columns=['Simultaneous batch']).drop_duplicates()
        df_seq = pd.DataFrame(batch_seq, columns=['Sequential batch']).drop_duplicates()
        df_con = pd.DataFrame(batch_con, columns=['Concurrent batch']).drop_duplicates()

    data = [['A', 10, 'a'], ['B', 12, 'b'], ['C', 13, 'c']]
    df = pd.DataFrame(data, columns=[' ', ' ', ' '])

    return render_template('dataframe.html', tables=[df_sim.to_html(classes='data'), df_seq.to_html(classes='data'),
                                                     df_con.to_html(classes='data')], titles=df.columns.values)


@social_network_analysis_app.route('/duplicate_activity')
def duplicate_activity():
    """Find the duplicate activities across different resources.

    Returns
    -------
    Dataframe
        List duplicate activities across different resources.
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()

    query.add(PQLColumn(name="caseId", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="activity", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="occurrences", query=f'COUNT("{model_name}"."{activity_column}")'))
    df = datamodel.get_data_frame(query)

    df['occ'] = df['occurrences'].apply(lambda cell: 1)

    df2 = df.groupby(['caseId', 'activity'], group_keys=True)['resource'].apply(lambda x: (x + ' ').sum()).reset_index()
    df3 = df.groupby(['caseId', 'activity'], group_keys=True).sum().reset_index()
    df4 = pd.merge(df2, df3, on=['caseId', 'activity'])

    result = df4.loc[df4['occ'] != 1]
    result = result.drop(labels='occ', axis=1)

    data = [['A', 10, 'a'], ['B', 12, 'b'], ['C', 13, 'c']]
    title = pd.DataFrame(data, columns=['', ' ', ' '])

    return render_template('dataframe.html', tables=[result.to_html(classes='data')], titles=title.columns.values)


@social_network_analysis_app.route('/roles_discovery')
def roles_discovery():
    """A role is a set of activities in the log that are executed by a similar (multi)set of resources.
    :return: Dataframe
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    datamodel = model.datamodels.find(model_name)
    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    df = datamodel.get_data_frame(query)
    roles = pm4py.discover_organizational_roles(df)
    role_acts = []
    role_res = []
    for i in roles:
        role_acts += [i.activities]
        role_res += [len(i.originator_importance)]

    result = pd.DataFrame({'Role Number': range(1, len(roles) + 1),
                           'Activities and Resources': role_acts,
                           'Number of resources': role_res})
    return render_template('dataframe.html', tables=[result.to_html(classes='data')], titles=result.columns.values)


@social_network_analysis_app.route('/organizational_mining')
def organizational_mining():
    """Discovery of behavior-related information specific to an organizational group (e.g. which activities are done by the group?).
    If the "org:group" attribute be there in the events, which describing the group that performed the event, you can use this function.

    :return: Dataframe
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    datamodel = model.datamodels.find(model_name)
    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))
    df = datamodel.get_data_frame(query)
    try:
        ld = local_diagnostics.apply_from_group_attribute(df, parameters={
            local_diagnostics.Parameters.GROUP_KEY: "org:group"})
    except:
        return render_template('empty.html', msg="There is no attributes in this event log describing the group that "
                                                 "performed the event")

    result = pd.DataFrame({'Group Relative Focus': ld["group_relative_focus"],
                           'Group Relative Stake': ld["group_relative_stake"],
                           'Group Coverage': ld["group_coverage"],
                           'Group Member Contribution': ld["group_member_contribution"]})
    return render_template('dataframe.html', tables=[result.to_html(classes='data')], titles=result.columns.values)


@social_network_analysis_app.route('/resource_activity_matrix_absolute')
def resource_activity_matrix_absolute():
    """Find the resource-activity matrix.

    Returns
    -------
    Dataframe
        Restructure Eventlog to Resource-activity Matrix.
    """
    try:
        matrix, _, col = RAM_restructure(sum=True, absolute=True)
    except ValueError:
        return redirect('/')

    return render_template('dataframe.html', tables=[matrix.to_html(classes='data')], titles=list(col))


@social_network_analysis_app.route('/resource_activity_matrix_percent')
def resource_activity_matrix_percent():
    """Find the resource-activity matrix.

    Returns
    -------
    Dataframe
        Restructure Eventlog to Resource-activity Matrix.
    """
    try:
        matrix, _, col = RAM_restructure(sum=True, absolute=False)
    except ValueError:
        return redirect('/')

    return render_template('dataframe.html', tables=[matrix.to_html(classes='data')], titles=list(col))


@social_network_analysis_app.route('/work_handover')
def work_handover():
    """Extract the handover of works .

    Returns
    -------
    Dataframe
        List all the activity-resource when handover happened
    """
    try:
        df = get_activity_resource()
    except ValueError:
        return redirect('/')

    df = df[df["source-resource"] != df["target-resource"]].sort_values(by='occurrences', ascending=0)
    return render_template('dataframe.html', tables=[df.to_html(classes='data')], titles=df.columns.values)


@social_network_analysis_app.route('/similar_activities')
def similar_activities():
    """how much similar is the work pattern between two resources.
    :return: Dataframe
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    datamodel = model.datamodels.find(model_name)
    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))

    df = datamodel.get_data_frame(query)

    sa_values = pm4py.discover_activity_based_resource_similarity(df)

    pm4py.view_sna(sa_values)
    result = pd.DataFrame(list(sa_values.connections.items()))
    result.columns = ['Resources', 'Similar Activities Metric']

    return render_template('dataframe.html', tables=[result.to_html(classes='data')], titles=result.columns.values)


@social_network_analysis_app.route('/working_together')
def working_together():
    """Find the duplicate activities across different resources.

    Returns
    -------
    Dataframe
        List duplicate activities across different resources.
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))
    df = datamodel.get_data_frame(query)

    wt_values = pm4py.discover_working_together_network(df)
    pm4py.view_sna(wt_values)

    return render_template('empty.html', msg='You got it! Check the new tab',)


@social_network_analysis_app.route('/subcontracting')
def subcontracting():
    """Find the duplicate activities across different resources.

    Returns
    -------
    Dataframe
        List duplicate activities across different resources.
    """
    try:
        model, datamodel, model_name, case_column, activity_column, timestamp_column, resource_column = get_celonis_settings()
    except ValueError:
        return redirect('/')

    query = PQL()
    query.add(PQLColumn(name="case:concept:name", query=f'"{model_name}"."{case_column}"'))
    query.add(PQLColumn(name="concept:name", query=f'"{model_name}"."{activity_column}"'))
    query.add(PQLColumn(name="org:resource", query=f'"{model_name}"."{resource_column}"'))
    query.add(PQLColumn(name="time:timestamp", query=f'"{model_name}"."{timestamp_column}"'))
    df = datamodel.get_data_frame(query)

    wt_values = pm4py.discover_subcontracting_network(df)
    pm4py.view_sna(wt_values)

    return render_template('empty.html', msg='You got it! Check the new tab.',)
